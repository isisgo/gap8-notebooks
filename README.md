# gap8-notebooks

Repository with Google Colab Python Notebooks used to preprocessing/cleaning and analysis of GAP8 tests data. This is part of work "Energy Efficiency Analysis and Characterization of the Gap8 Platform".

- **Preprocessing**: This notebook gets all the raw data from a folder and write it as clean csv's into another. (Google Drive folders: raw_data and clean_data). At the end of this notebook, it is possible to save all the cleaned and concated data as an only csv. This file is saved into another Google Drive folder that always update the same csv with recent data (Google Drive folder: preprocessed_data);
- **Efficiency_Analysis**: This notebook reads the csv from the preprocessed_data folder and create the main dataframe for the analysis. Contains energy, time and power graphs.
- **Error_Analysis**: This notebook reads all data collected from several tests to observe errors and lockups.
